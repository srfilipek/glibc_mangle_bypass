# A glibc Pointer Mangle Bypass
A POC showing how the frame pointer can be used to bypass pointer manging in setjmp/longjmp for ARM 32-bit.

The current glibc (2.28 at the time of discovery) setjmp/longjmp system mangles / protects the stack pointer and link register in the `jmp_buf` structure. However, this is not always sufficient to prevent control flow attacks with ARM systems; the frame pointer should be protected as well. In some situations, frame pointer manipulation can trivially lead to a stack pivot and then control over the program counter.

A vulnerable situation looks like:
```
    sub     sp, fp, #N     <-- modified fp used to set sp
    pop     {..., fp, pc}  <-- modified sp used to set pc
```

The susceptibility an application to this vulnerability depends on the compiler's use of the frame pointer (if used at all), so the attack is not universal. But, for example, there are over 190 instances of the above pattern in glibc 2.24 libc.so on the Pi. Note that the actual register used as the "frame pointer" can also change based on the ABI and if Thumb mode is used instead.

This PoC can be compiled with either GCC or Clang. Tested with glibc 2.24, 2.28, and the latest master at the time of discovery (648279f4af). The specific version of gcc or clang should not matter. This was confirmed to work in qemu-arm on Linux and on the Raspberry Pi 3.

To compile:

gcc: arm-linux-gnueabihf-gcc -mcpu=cortex-a7 -marm -g -O0 -fno-omit-frame-pointer main.c -o main

clang: clang -mcpu=cortex-a7 -marm -g -O0 -fno-omit-frame-pointer -target arm-linux-gnueabihf main.c -o main

Note that the optimization and frame pointer flags are simply to help ensure the vulnerable pattern is emitted for this very simple test case -- the vulnerable pattern can occur regardless.

## Disclosure
This issue was reported in April of 2019: https://sourceware.org/bugzilla/show_bug.cgi?id=24432
